MAKE_ESP ?= $(HOME)/src/ESP/makeEspArduino/makeEspArduino.mk

UPLOAD_SPEED ?= 115200
UPLOAD_VERB ?= -v

CHIP ?= esp32
ifeq ($(CHIP), esp32)
	ESP_ROOT := $(HOME)/src/ESP/esp32
	BOARD := esp32s2
	SKETCH := firmware32.ino
else
	ESP_ROOT := $(HOME)/src/ESP/esp8266
	BOARD := nodemcu
	SKETCH := firmware8266.ino
endif

ifdef DHTTYPE
BUILD_EXTRA_FLAGS = "-D DHTTYPE=$(DHTTYPE)"
endif

include $(MAKE_ESP)

.PHONY: publish

publish:
	cp $(MAIN_EXE) $(BUILD_ROOT)/firmware-$$(grep VERSION invars.h | awk '{print $$NF}').bin && echo "$(BUILD_ROOT)/firmware-$$(grep VERSION invars.h | awk '{print $$NF}').bin published"
