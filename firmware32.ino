// Uncomment this to activate DEBUG mode.
#define DEBUG


#include <WiFi.h>
#include <HTTPClient.h>
#include <DHTesp.h>
#include <base64.h>

#include "invars.h"

// The LOG macro sends data to computer, in DEBUG mode only.
#ifdef DEBUG
#define LOGBEGIN( ... ) Serial.begin(115200)
#define LOG(...) Serial.print(__VA_ARGS__)
#define LOGLN(...) Serial.println(__VA_ARGS__)
#define LOGF(...) Serial.printf(__VA_ARGS__)
#define LOGFLUSH( ... ) Serial.flush()
#else
#define LOGBEGIN( ... )
#define LOG(...)
#define LOGLN(...)
#define LOGf(...)
#define LOGFLUSH( ... )
#endif


#define μS_to_S 1000000


// Sensor types
#define LIPSENSOR_SENSOR_DHT22


// Errors
#define LIPSENSOR_ERROR_WIFI_CONNECTION     (-1)
#define LIPSENSOR_ERROR_PORTAL_CONNECTION   (-2)
#define LIPSENSOR_ERROR_CANNOT_SEND_WEATHER (-3)


int connectToWiFiInvites() {
    // Work variables
    unsigned long start_time;

    LOG("Connecting to Wi-Fi: ");
    LOGLN(WIFI_SSID);
    start_time = millis();
    WiFi.begin(WIFI_SSID.c_str());
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        if (millis() - start_time > WIFI_TIMEOUT) {
            LOGLN("");
            LOGLN("\nConnection timeout");
            return LIPSENSOR_ERROR_WIFI_CONNECTION;
        }
        LOG(".");
    }
    WiFi.persistent(true);
    WiFi.setAutoConnect(true);
    WiFi.setAutoReconnect(true);
    LOGLN("");
    LOGLN("WiFi connected.");
    LOG("  IP address: ");
    LOGLN(WiFi.localIP());
    return 0;
}


int checkWiFiInvitesConnection(unsigned long timeout) {
    // Work variables
    unsigned long start_time;

    if (WiFi.isConnected()) {
        LOGLN("WiFi still connected");
        return 0;
    }

    LOGLN("WiFi connection lost. Try reconnect.");
    start_time = millis();
    WiFi.reconnect();
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        if (millis() - start_time > timeout) {
            LOGLN("");
            LOGLN("\nConnection timeout");
            return LIPSENSOR_ERROR_WIFI_CONNECTION;
        }
        LOG(".");
        WiFi.reconnect();
    }
    return 0;
}


int loginToPortalInvites() {
    // Parameters.
    String url = "https://portailwifi.ens-lyon.fr/portal_api.php";
    // Fool server.
    char* user_agent = "Mozilla/5.0 (Linux; Android 4.2.2; Nexus 7 Build/JDQ39) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.49 Safari/537.31";
    // Work variables.
    HTTPClient http;
    String request_data;
    String payload;
    String cookie;
    const char* headerKeys[] = {"Set-Cookie"};

    // Start HTTPS connection.
    http.begin(url, WIFI_CA);

    // Add some headers to fool the server.
    http.setUserAgent(user_agent);
    http.addHeader("Content-type",   "application/x-www-form-urlencoded");
    http.collectHeaders(headerKeys, 1);

    request_data += "action=authenticate";
    request_data += "&login=" + WIFI_USER;
    request_data += "&password=" + WIFI_PASS;
    request_data += "&policy_accept=true";

    // Make POST Request
    LOG("Connecting to captive portal: ");
    LOGLN(url);

    int http_code = http.POST(request_data);

    if (http_code < 0) {
        LOGLN("  !! HTTP FAILURE !!: ");
        LOGF("  Error code: %d ", http_code);
        LOGLN(http.errorToString(http_code));
        http.end();
        return LIPSENSOR_ERROR_PORTAL_CONNECTION;
    }
    LOGF("  Status code: %d\n", http_code);

    cookie = http.header("Set-Cookie");
    LOGLN("  Set-Cookie: " + cookie);

    LOGLN("Connected to captive portal.");
    http.end();
    return 0;
}


struct WeatherMessage {
    float humidity;
    float temperature;
    float heat_index;
    float dew_point;
    float voltage;
};


WeatherMessage prepareMessage(DHTesp dht) {
    WeatherMessage message;

    message.humidity = dht.getHumidity();
    message.temperature = dht.getTemperature();
    message.heat_index = dht.computeHeatIndex(message.temperature, message.humidity);
    message.dew_point = dht.computeDewPoint(message.temperature, message.humidity);
    message.voltage = analogRead(dht.getPin());

    LOGLN("Collected data:");
    LOGF("  Humidity:  %9f", message.humidity);
    LOGF("  Temperature: %9f\n", message.temperature);
    LOGF("  Dew point: %9f", message.dew_point);
    LOGF("  Heat index:  %9f\n",message.heat_index);
    LOGF("  Voltage: %9f\n", message.voltage);
    return message;
}


int sendMessage(String dataid, WeatherMessage message) {
    // Work variables.
    HTTPClient http;
    String url;
    String auth;

    url += DATA_URL;
    url += "?h=" + String(message.humidity);
    url += "&t=" + String(message.temperature);
    url += "&v=" + String(message.voltage);
    url += "&id=" + dataid;
    url += "&vers=" + VERSION;

    // Start HTTP connection.
    http.begin(DATA_HOST.c_str(), DATA_PORT, url, DATA_CA);

    auth = DATA_USER + ":" + DATA_PASS;
    http.addHeader("Authorization", "Basic " + base64::encode(auth));

    // Make GET Request
    LOGLN("Sending weather.");
    int http_code = http.GET();

    if (http_code < 0) {
        LOGLN("  !! HTTP FAILURE !!: ");
        LOGF("  Error code: %d ", http_code);
        LOGLN(http.errorToString(http_code));
        http.end();
        return LIPSENSOR_ERROR_CANNOT_SEND_WEATHER;
    }
    LOGF("  Status code: %d\n", http_code);
    if (http_code != HTTP_CODE_OK) {
        LOGLN("Message: ");
        LOGLN(http.getString());
    }

    return 0;
}


void blink(int duration) {
        digitalWrite(LED_BUILTIN, HIGH);
        delay(duration);
        digitalWrite(LED_BUILTIN, LOW);
}
void blink_forever(int duration, int step) {
    while (true) {
        blink(duration);
        delay(step);
    }
}


// GLOBAL CONSTANTS

const uint8_t SENSOR_PIN = 17;


// GLOBAL VARIABLES

RTC_DATA_ATTR uint boot_count = 0;
String dataid;  // Sensor ID, which is its MAC address.
DHTesp dht;  // Interface to sensor.
WeatherMessage message;


void setup() {
    boot_count++;

    // Setup board LED
    pinMode(LED_BUILTIN, OUTPUT);

    LOGBEGIN(); // Setup communication with computer.
    LOGF("Starting up for the %d time\n", boot_count);

    // Setup the wake up interval.
    esp_sleep_enable_timer_wakeup(SLEEP_TIME * μS_to_S);

    // Setup the data id
    dataid = WiFi.macAddress();
    dataid.replace(":", "");

    if (boot_count <=1) {
        LOGF("  Dataid: %s\n", dataid);
    }

    // Setup thermal sensor.
#ifdef LIPSENSOR_SENSOR_DHT22
    dht.setup(SENSOR_PIN, DHTesp::DHT22);
#endif

    if (boot_count <= 1) switch (dht.getModel()) {

        case DHTesp::DHT22:
            LOGLN("  Sensor is DHT22");
            LOGF("    Humidity range: %d", dht.getLowerBoundHumidity());
            LOGF(" -- %d\n", dht.getUpperBoundHumidity());
            LOGF("    Temperature range: %d", dht.getLowerBoundTemperature());
            LOGF(" -- %d\n", dht.getUpperBoundTemperature());
            LOGF("    Humidity number of decimals: %d\n", dht.getNumberOfDecimalsHumidity());
            LOGF("    Temperature number of decimals: %d\n", dht.getNumberOfDecimalsTemperature());
            break;

        default:
            LOGLN("  No sensor");
            break;
    }

    if (connectToWiFiInvites() == 0 && loginToPortalInvites() == 0)
       switch (dht.getModel()) {

        case DHTesp::DHT22:
            message = prepareMessage(dht);
            sendMessage(dataid, message);
            // Slow blink when it is working fine
            for (uint r = 0; r < 1; r++) { blink(1000); delay(1000); }
            break;

        default:
            // Fast blink when something is not normal.
            for (uint r = 0; r < 5; r++) { blink(200); delay(200); }
            break;
    } else {
        LOGLN("Unable to connect WiFi.");
    }

    LOGLN("Going to sleep now.");
    LOGLN("");
    LOGFLUSH();
    WiFi.disconnect();
    esp_deep_sleep_start();
}

void loop() {}

// vim: ft=arduino
// vim: et sw=4 sts=4 sw=4
