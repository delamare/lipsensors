#define STRINGIFY(X) #X
#define STR(X) STRINGIFY(X)

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266httpUpdate.h>

#include <base64.h>

#include "invars.h"

#ifndef DHTTYPE
#define DHTTYPE DHT22  // Large, white sensor
//#define DHTTYPE DHT12  // Small, blue sensor
#endif

#include <DHT.h>
#include <Wire.h>  // Weird but works.
#include <WEMOS_DHT12.h>

#ifdef DEBUG
#define PRINT(...) Serial.print(__VA_ARGS__)
#define PRINTLN(...) Serial.println(__VA_ARGS__)
#else
#define PRINT(...)
#define PRINTLN(...)
#endif

String available_version = "-1";

String wificookie = "";

String dataauth = DATA_USER + ":" + DATA_PASS;

String dataid = "";  // To identify board in HTTP communications.
float datah = NAN;
float datat = NAN;
float dataa = NAN;
float hic = NAN;

int starttime = -1;

#if DHTTYPE == DHT22
#define DHTPIN D4
DHT dht(DHTPIN, DHTTYPE, 15);
#elif DHTTYPE == DHT12
DHT12 dht;
#endif

//
// Configuration Wifi Invites
//

const String url_invites = "https://portailwifi.ens-lyon.fr/portal_api.php";
const String userAgent_invites = "Mozilla/5.0 (Linux; Android 4.2.2; Nexus 7 Build/JDQ39) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.49 Safari/537.31";


int wifiLoginInvites() {

  PRINTLN("Connecting to "+WIFI_SSID);

  if (WiFi.SSID() != WIFI_SSID) {
    PRINTLN("  Setting Wifi parameters");
    WiFi.mode(WIFI_STA);
    WiFi.begin(WIFI_SSID.c_str());
    WiFi.persistent(true);
    WiFi.setAutoConnect(true);
    WiFi.setAutoReconnect(true);
  }

  int tries = 0;
  PRINT("  ");
  while (WiFi.status() != WL_CONNECTED && tries < 60) {
    PRINT(".");
    tries++;
    delay(500);
  }
  if (WiFi.status() != WL_CONNECTED) {
    PRINTLN("  !! CANNOT CONNECT !!");
    WiFi.disconnect();
    return -1;
  }
  PRINTLN("  WiFi connected");
  PRINT("  IP address: ");
  PRINTLN(WiFi.localIP());

  PRINTLN("Connecting to captive portal...");
  int retcode;
  String res;
  HTTPClient http;
  const char* headerKeys[] = {"Set-Cookie"};

  http.begin(url_invites, DATA_FINGERPRINT);
  http.setUserAgent(userAgent_invites);
  http.addHeader("Content-type",   "application/x-www-form-urlencoded");
  http.collectHeaders(headerKeys, 1);
  retcode = http.POST("action=authenticate&login=" + WIFI_USER + "&password=" + WIFI_PASS + "&policy_accept=true");
  res = http.getString();
  wificookie = http.header("Set-Cookie");
  PRINTLN("  Url: " + url_invites);
  PRINTLN("  Status code: " + retcode);
  PRINTLN("  Set-Cookie: " + wificookie);
  PRINTLN("  Message:");
  PRINTLN("    " + res);

  if (retcode != 200) {
    PRINT("  !! HTTP FAILURE !!: ");
    PRINTLN(http.errorToString(retcode));
    return -2;
  }
  http.end();
  return 0;
}

int wifiUnloginInvites() {

  PRINTLN("Unconnecting from captive portal");
  int retcode;
  String res;
  HTTPClient http;

  http.begin(url_invites, DATA_FINGERPRINT);
  http.setUserAgent(userAgent_invites);
  http.addHeader("Content-type", "application/x-www-form-urlencoded");
  http.addHeader("Cookie", wificookie);

  retcode = http.POST("action=disconnect&login=" + WIFI_USER);
  res = http.getString();
  PRINTLN("  Url: " + url_invites);
  PRINTLN("  Status code: " + retcode);
  PRINTLN("  Message:");
  PRINTLN("    " + res);

  if (retcode != 200) {
    PRINT("  !!HTTP FAILURE !!: ");
    PRINTLN(http.errorToString(retcode));
    return -2;
  }
  http.end();
  return 0;
}

//int wifiLoginWPA() {
//
//  PRINTLN("Connecting to " + WIFI_SSID);
//
//  int tries = 0;
//
//  if (WiFi.SSID() != WIFI_SSID) {
//    PRINTLN("  Setting Wifi parameters");
//    WiFi.mode(WIFI_STA);
//    WiFi.begin(WIFI_SSID.c_str(), wifipass.c_str());
//    WiFi.persistent(true);
//    WiFi.setAutoConnect(true);
//    WiFi.setAutoReconnect(true);
//  }
//  PRINT("  ");
//  while (WiFi.status() != WL_CONNECTED) {
//    tries += 1;
//    delay(500);
//    PRINT(".");
//    if (tries > 60) {
//      PRINTLN("  Cannot connect");
//      WiFi.disconnect();
//      return -1;
//    }
//  }
//  PRINTLN("");
//  PRINTLN("  WiFi connected");
//  PRINT("  IP address: ");
//  PRINTLN(WiFi.localIP());
//  return 0;
//}


int readSensor() {

  PRINT("Reading sensor");
#if DHTTYPE == DHT22
  PRINTLN(" (DHT22)");
#elif DHTTYPE == DHT12
  PRINTLN(" (DHT12)");
#endif

  dataa = analogRead(A0);

  int tries = 0;
  PRINT("  ");
  do {
#if DHTTYPE == DHT22
    dht.begin();
    datah = dht.readHumidity();
    datat = dht.readTemperature();
    // Compute heat index in Celsius (isFahreheit = false)
    hic = dht.computeHeatIndex(datat, datah, false);

#elif DHTTYPE == DHT12
    if (dht.get() == 0) {
      datah = dht.humidity;
      datat = dht.cTemp;
    }
    // Cannot compute heat index with this sensor
    hic = NAN;
#endif
    PRINT(".");
    tries ++;
    delay(500);
  } while ((isnan(datah) && isnan(datat)) && tries < 10);

  if ((isnan(datah) || isnan(datat))) {
    PRINTLN("  !! CANNOT COLLECT ALL DATA !!");
  }

  PRINT("  Humidity: ");
  PRINT(datah);
  PRINT(" %  ");
  PRINT("Temperature: ");
  PRINT(datat);
  PRINT(" C  ");
  PRINT("Heat index: ");
  PRINT(hic);
  PRINT(" C  ");
  PRINT("Input Voltage: ");
  PRINT(dataa);
  PRINT(" V  ");
  PRINT("Board Id: ");
  PRINT(dataid);
  PRINTLN("");
}


int sendData() {

  PRINTLN("Sending data");

  int retcode;
  String res;
  WiFiClientSecure client;

  PRINT("  Connecting to " + DATA_HOST + "... ");
  //client.setFingerprint(datacert);
  client.setInsecure();
  if (!client.connect(DATA_HOST, DATA_PORT)) {
    PRINTLN("!! CONNECTION FAILED !!");
    return -1;
  }
  PRINTLN("OK");

  String url = DATA_URL + "?h=" + datah + "&t=" + datat + "&v=" + dataa + "&id=" + dataid + "&vers=" + VERSION;

  PRINT("  Sending Url-encoded data: ");
  PRINTLN(url);
  String req = String("GET ") + url + " HTTP/1.1\r\n" +
               "Host: " + DATA_HOST + "\r\n" +
               "Connection: close\r\n" +
               "Authorization: Basic "+ base64::encode(dataauth, 0) +"\r\n" +
               "\r\n";
  client.print(req);

  PRINTLN("  Response:");
  while (client.available()) {
    String line = client.readStringUntil('\n');
    PRINTLN("    "+line);
  }

  available_version = res;
  return 0;
}

class MyHTTPUpdate : public ESP8266HTTPUpdate
{
  public:
    HTTPUpdateResult update(HTTPClient& http, const String& currentVersion) {
      return ESP8266HTTPUpdate::handleUpdate(http, currentVersion, false);
    }
};
int httpUpdate() {

  PRINTLN("Updating");

  HTTPClient http;
  MyHTTPUpdate httpupdate;
  String res;
  int ret;

#ifdef DATACERT
  String url = "https://" + dataauth + DATA_HOST + "/update";
  http.begin(url, datacert);
#else
  String url = "http://" + dataauth + DATA_HOST + "/update";
  http.begin(url);
#endif
  t_httpUpdate_return httpret = httpupdate.update(http, VERSION);

  switch(httpret) {
    case HTTP_UPDATE_FAILED:
        PRINTLN("  Update failed.");
        res = http.getString();
        PRINTLN("  MSG = " + res);
        ret = -2;
        break;
    case HTTP_UPDATE_NO_UPDATES:
        PRINTLN("  No update.");
        res = http.getString();
        PRINTLN("  MSG = " + res);
        ret = -1;
        break;
    case HTTP_UPDATE_OK:
        PRINTLN("  Update OK"); // may not called we reboot the ESP
        ret = 0;
        break;
  }
  http.end();
  return ret;
}

void setup() {

  Serial.begin(115200);

  PRINTLN("Starting up");
  PRINT("Version is ");
  PRINTLN(VERSION);

  starttime = millis();

  // Get self ID from its own MAC address.
  dataid = WiFi.macAddress();
  dataid.replace(":", "");

  PRINT("Dataid is ");
  PRINTLN(dataid);

#if DHTTYPE == DHT22
  PRINTLN("Sensor is DHT22 (White)");
#elif DHTTYPE == DHT12
  PRINTLN("Sensor is DHT12 (Blue)");
#endif
}


void loop() {

  if (wifiLoginInvites() == 0) {
  //if (wifiLoginWPA() == 0) {

    readSensor();
    sendData();
    if (available_version > VERSION) {
        httpUpdate();
    }

    wifiUnloginInvites();
  }

  PRINT("Sleeping after ");
  PRINTLN(millis()-starttime);
  ESP.deepSleep(SLEEP_TIME * 60 * 1000000 - (millis()-starttime)*1000, WAKE_RF_DEFAULT);
  delay(5000);
}

// vim: ft=arduino
// vim: et sw=4 sts=4 sw=4
