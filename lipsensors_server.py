#!/usr/bin/env python

from flask import request, Response, Flask, send_from_directory
from pyonf import pyonf
import glob
import os
import socket

default_opts = """
username: null
password: null
sensors_map: {}
update_interval: 300
flask_host: 127.0.0.1
flask_port: 8000
collectd_socket_path: /var/run/collectd-unixsock
collectd_args_map:
  t: {plugin_instance: temperature, vtype: temperature}
  h: {plugin_instance: humidity, vtype: percent}
  v: {plugin_instance: VCCin, vtype: voltage}
"""

conf = pyonf(default_opts)
app = Flask(__name__)


def get_firmware(nodeid=None, nodeversion=False):
    firmwares = []
    if nodeid:
        firmwares += sorted(glob.glob(str('firmware/%s-*.bin' % nodeid)),
                            key=lambda fname: fname.split('-')[1],
                            reverse=True)
    firmwares += sorted(glob.glob(str('firmware/firmware-*.bin')),
                        key=lambda fname: fname.split('-')[1],
                        reverse=True)
    print("Firmware available: "+str(firmwares))
    if firmwares:
        firmwares = [os.path.basename(firmware)
                     for firmware in firmwares
                     if not nodeversion
                     or firmware.split('-')[-1].split('.')[0] > nodeversion]
        return firmwares[0]
    else:
        return "0"


def log_data_to_collectd(args):
    sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    try:
        sock.connect(conf['collectd_socket_path'])
    except Exception as e:
        print("Unable to open collectd socket, ignoring values...")
        print(e)
        return None

    values = []
    host = conf['sensors_map'].get(args.get('id')) \
           or args.get('id') \
           or "UNKNOWN"
    instance = "lipsensors"
    timestamp = "N"
    for arg in args:
        if conf['collectd_args_map'].get(arg):
            plugin_instance = conf['collectd_args_map'][arg].get('plugin_instance')
            vtype = conf['collectd_args_map'][arg].get('vtype')
            vtype_instance = conf['collectd_args_map'][arg].get('vtype_instance')
            try:
                value = float(args.get(arg))
            except ValueError:
                print("Unable to parse value for arg %s, ignoring..." % arg)
            else:
                values.append("PUTVAL %s/%s%s/%s%s interval=%d %s:%.2f" %
                              (host,
                               instance, "-"+plugin_instance if plugin_instance else "",
                               vtype, "-"+vtype_instance if vtype_instance else "",
                               conf['update_interval'], timestamp, value))
    print("\n".join(values)+"\n")
    sock.send(str.encode(str("\n".join(values)+"\n"), encoding='ascii'))
    sock.close()


def auth(request):
    if conf.get('username'):
        if not request.authorization or \
           not request.authorization.username == conf.get('username') or \
           not request.authorization.password == conf.get('password'):
               return False
    return True


@app.route('/data', methods=['GET'])
@app.route('/sensors/data', methods=['GET'])
def data():
    if not auth(request):
        return Response('Wrong authentication\n', 401,
                        {'WWW-Authenticate': 'Basic realm="Login Required"'})

    print(request.args)
    log_data_to_collectd(request.args)

    f = get_firmware(request.args.get('id'))
    if f:
        return Response(f.split('-')[-1].split('.')[0])
    else:
        return "0"


@app.route('/update', methods=['GET'])
@app.route('/sensors/update', methods=['GET'])
def update():
    if not auth(request):
        return Response('Wrong authentication\n', 401,
                        {'WWW-Authenticate': 'Basic realm="Login Required"'})
    if not request.headers.get('X_ESP8266_STA_MAC'):
        print('Could not verify your update identifier for that URL')
        return Response('Could not verify your update identifier for that URL', 403)
    if not request.headers.get('X_ESP8266_VERSION'):
        print('Could not verify your update version for that URL')
        return Response('Could not verify your update version for that URL', 403)
    firmware = get_firmware(
            request.headers.get('X_ESP8266_STA_MAC').replace(':', ''),
            request.headers.get('X_ESP8266_VERSION')
            )
    if firmware:
        return send_from_directory('firmware', firmware, as_attachment=True)
    else:
        return Response('No firmware available for update', 404)


if __name__ == '__main__':
    app.debug = True
    app.run(host=conf['flask_host'], port=conf['flask_port'])
